import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'visualisation',
  template: `
    <div>
      <label for="currentState">Current state</label>
      <input id="currentState" [(ngModel)]="currentState" />
      <button (click)="start(currentState)">Go</button>
      <pre>{{ displayedWorkItems | json }}</pre>
      <table *ngIf="workItems">
        <tr>
          <td>
            <div class="flex-container">
              <div class="flex-item" *ngFor="let workItem of displayedWorkItems">
                  <strong>{{ workItem.id }}</strong>
                  <br />{{ workItem.type }}
                  <br />state: {{ workItem.state }}
              </div>
            </div>
           </td>
          <td>
            <div class="flex-container">
              <div class="flex-item" *ngFor="let workItem of displayedWorkItems">
                  <strong>{{ workItem.id }}</strong>
                  <br />type: {{ workItem.type }}
                  <br />state: {{ workItem.state }}
              </div>
            </div>
          </td>
          <td>
            <div class="flex-container">
              <div class="flex-item" *ngFor="let workItem of displayedWorkItems">
                  <strong>{{ workItem.id }}</strong>
                  <br />type: {{ workItem.type }}
                  <br />state: {{ workItem.state }}
              </div>
            </div>
          </td>
        </tr>
      </table>
      <ul>
        <li *ngFor="let workItem of displayedWorkItems">
          <strong>{{ workItem.id }}</strong>
            type: {{ workItem.type }}
            state: {{ workItem.state }}
            rev: {{ workItem.rev }}
            id: {{ workItem.id }}
            changedDate: {{ workItem.changedDate }}
            <pre>{{ workItem | json }}</pre>
        </li>
      </ul>
    </div>
  `,
  styles: [
    `
    td {
      width: 600px;
    }

    tr {
      vertical-align: top;
    }

    .flex-container {
      display: flex;
      flex-wrap: wrap;
    }

    .flex-item {
      width: 150px;
      height: 75px;
      background-color: yellow;
      margin: 4px;
      padding: 2px;
    }
    `
  ]
})
export class Visualisation implements OnInit {
  private errorMessage: any;
  private workItems: any[];
  private currentState: Number = 1;

  private displayedWorkItems: any[];

  private interval: number = 3600; // 1 hour
  private startDate: Date = new Date(2016, 7, 30);
  private previousDate: Date;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.currentState = 1;
    this.dataService.getMockData().subscribe(workItems => {
      this.workItems = workItems;
      this.displayedWorkItems = this.workItems.filter(wi => new Date(wi.changedDate) < this.startDate);
    });
  }

  start(newState) {
    newState = +newState;
    var newDate = this.addSeconds(this.startDate, this.interval * newState);

    if (!this.previousDate) {
      this.previousDate = this.startDate;
    }

    var deltaWorkItems = this.workItems.filter(wi => new Date(wi.changedDate) > this.previousDate && new Date(wi.changedDate) < newDate);

    deltaWorkItems.forEach(newWorkItem => {
      // modify the changed work items
      this.displayedWorkItems = this.displayedWorkItems.map(existingWorkItem => {
        if (newWorkItem.id === existingWorkItem.id) {
          return newWorkItem;
        }
        return existingWorkItem;
      });
      // TODO Add the new workitems to displayedWorkItems
    });

    this.previousDate = newDate;
  }

  private addSeconds(date, seconds) {
    var result = new Date(date);
    return new Date(result.getTime() + seconds * 1000);
  }

}
