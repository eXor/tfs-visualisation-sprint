import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';


@Injectable()
export class DataService {

  constructor(private http: Http) { }

  getData(): Observable<any[]> {
    return this.http.get("http://localhost:5000/api/data/Timesheet/Timesheet%5C2016%20-%20Q4%5C2016%20-%2011%5CSprint%2097")
      .map(res => res.json())
      .catch(error => {
        console.error(error);
        return Observable.throw(error);
      })
  }

  getMockData(): Observable<any[]> {
    return this.http.get("http://localhost:5000/api/data/mock")
      .map(res => res.json())
      .catch(error => {
        console.error(error);
        return Observable.throw(error);
      })
  }

}
