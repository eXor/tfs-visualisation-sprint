import { VisualisatiePage } from './app.po';

describe('visualisatie App', function() {
  let page: VisualisatiePage;

  beforeEach(() => {
    page = new VisualisatiePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
