using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using System.Linq;
using System;
using System.Collections.Generic;

namespace Api.Services
{
    public class DataService
    {
        private List<OurWorkItem> _workItems;

        public DataService()
        {
            _workItems = new List<OurWorkItem>();
        }

        public IEnumerable<OurWorkItem> GetData(string teamProject, string iterationPath)
        {
            var collectionUri = "http://tfs.raet.nl:8080/tfs/DefaultCollection";

            // create TfsTeamProjectCollection instance using default credentials
            using (TfsTeamProjectCollection tpc = new TfsTeamProjectCollection(new Uri(collectionUri)))
            {
                // get the WorkItemStore service
                WorkItemStore workItemStore = tpc.GetService<WorkItemStore>();

                // get the project context for the work item store
                Project workItemProject = workItemStore.Projects[teamProject];

                // search for the 'My Queries' folder
                QueryFolder myQueriesFolder = workItemProject.QueryHierarchy.FirstOrDefault(qh => qh is QueryFolder && qh.IsPersonal) as QueryFolder;
                if (myQueriesFolder != null)
                {
                    string queryName = "TFSVisualisation";

                    var newBugsQuery = new QueryDefinition(queryName,
                        "SELECT [System.Title] FROM workitems" +
                        " WHERE ([System.IterationPath] = '" + iterationPath + "')" +
                        " AND  ([Team Project] = '" + teamProject + "')" +
                        // " AND ([System.Id] = 256604)" +
                        // " AND  ([System.Id] = 256549)" +
                        // " asof '19-09-2016 10:30'"
                        ""
                    );

                    // run the 'SOAP Sample' query
                    WorkItemCollection workItems = workItemStore.Query(newBugsQuery.QueryText);

                    foreach (WorkItem workItem in workItems)
                    {
                        foreach (Revision revision in workItem.Revisions)
                        {
                            if (workItem.Type.Name == "Product Backlog Item")
                            {
                                _workItems.Add(new Pbi()
                                {
                                    State = (string)revision.Fields["State"].Value,
                                    Rev = (int)revision.Fields["Rev"].Value,
                                    Id = (int)revision.Fields["ID"].Value,
                                    ChangedDate = (DateTime)revision.Fields["Changed Date"].Value
                                });
                            }

                            if (workItem.Type.Name == "Task")
                            {
                                _workItems.Add(new Task()
                                {
                                    State = (string)revision.Fields["State"].Value,
                                    Rev = (int)revision.Fields["Rev"].Value,
                                    Id = (int)revision.Fields["ID"].Value,
                                    ChangedDate = (DateTime)revision.Fields["Changed Date"].Value
                                });
                            }
                        }
                    }
                }
            }
            _workItems = _workItems.Distinct(new WorkItemComparer()).ToList();
            return _workItems.OrderBy(x => x.ChangedDate);
        }
    }

    class WorkItemComparer : System.Collections.Generic.IEqualityComparer<OurWorkItem>
    {
        // Products are equal if their names and product numbers are equal.
        public bool Equals(OurWorkItem x, OurWorkItem y)
        {

            //Check whether the compared objects reference the same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check whether the products' properties are equal.
            return x.Id == y.Id && x.State == y.State;
        }

        public int GetHashCode(OurWorkItem workItem)
        {
            //Check whether the object is null
            if (Object.ReferenceEquals(workItem, null)) return 0;

            //Get hash code for the Name field if it is not null.
            int hashProductName = workItem.Id == null ? 0 : workItem.Id.GetHashCode();

            //Get hash code for the Code field.
            int hashProductCode = workItem.State.GetHashCode();

            //Calculate the hash code for the product.
            return hashProductName ^ hashProductCode;
        }
    }

}