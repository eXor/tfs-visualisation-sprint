using System.Collections.Generic;
using Api.Services;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    public class DataController : Controller
    {
        private DataService _dataService;

        public DataController(DataService dataService)
        {
            _dataService = dataService;
        }

        [HttpGet]
        public string Get(){
            this.Response.ContentType = "text/html";
            return "<a href=\"http://localhost:5000/api/data/Timesheet/Timesheet%5C2016%20-%20Q4%5C2016%20-%2011%5CSprint%2097\">Click here</a>";
        }

        // http://localhost:5000/api/data/Timesheet/Timesheet%5C2016%20-%20Q4%5C2016%20-%2011%5CSprint%2097
        // GET api/data/Timesheet/Timesheet%5C2016%20-%20Q4%5C2016%20-%2011%5CSprint%2097
        [HttpGet]
        [Route("{teamProject}/{*iterationPath}")]
        public IEnumerable<OurWorkItem> Get(string teamProject, string iterationPath)
        {
            return _dataService.GetData(teamProject, iterationPath);
        }

        [HttpGet]
        [Route("mock")]
        public string GetMock(){
            Response.ContentType = "application/json";
            return System.IO.File.ReadAllText(@"mock.txt");
        }

    }
}