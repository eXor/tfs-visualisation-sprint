using System;

public abstract class OurWorkItem
{
    public string Type { get; set; }
    public string State { get; set; }
    public int Rev { get; set; }
    public int Id { get; set; }
    public DateTime ChangedDate { get; set; }
}

public class Pbi : OurWorkItem
{
    public Pbi()
    {
        Type = "Product Backlog Item";
    }
}

public class Task : OurWorkItem
{
    public Task()
    {
        Type = "Task";
    }
}