export interface Pbi {
  id: number;
  name: string;
  status: PbiStatus;
  date: Date;
  tasks: Array<Task>;
}

export interface Task {
  id: number;
  name: string;
  date: Date;
  status: TaskStatus;
}

export enum PbiStatus {
  New,
  Approved,
  Committed,
  Done,
  Removed
}

export enum TaskStatus {
  Todo,
  InProgress,
  Done,
  Removed
}
