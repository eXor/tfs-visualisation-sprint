import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';

import { DataService } from './services';
import * as T from '../../models';

@Component({
  selector: 'visualisation',
  templateUrl: 'visualisation.component.html',
  styles: [
    `
    table {
      border: gray solid 1px;
    }

    td, th {
      border: lightgray solid 1px;
      width: 200px;
      height: 150px;
      vertical-align: top;
      text-align: left;
    }

    th {
      height: 40px;
      text-align: center;
    }
    `
  ]
})
export class Visualisation implements OnInit {
  private sliderValue = 1;
  private timelapse: Array<any>;
  private highestKeyframe: number;

  constructor() { }

  pbiStatus(value) {
    return 'blaat';
  }

  ngOnInit() {
    this.timelapse = [
      {
        keyframe: 1,
        data: [
          {
            id: 1,
            name: 'The first pbi',
            status: T.PbiStatus.New,
            date: '2016-09-06 16:40',
            tasks: [],
            color: 'red'
          }
        ]
      },
      {
        keyframe: 2,
        data: [
          {
            id: 1,
            name: 'The first pbi',
            status: T.PbiStatus.Approved,
            date: '2016-09-08 12:33',
            tasks: [],
            color: 'red'
          }
        ]
      },
      {
        keyframe: 3,
        data: [
          {
            id: 1,
            name: 'The first pbi',
            status: T.PbiStatus.Committed,
            date: '2016-09-12 10:00',
            tasks: [],
            color: 'red'
          }
        ]
      },
      {
        keyframe: 4,
        data: [
          {
            id: 1,
            name: 'The first pbi',
            color: 'red',
            status: T.PbiStatus.Committed,
            date: '2016-09-12 10:00',
            tasks: [
              {
                id: 1,
                name: 'taak 1',
                color: 'blue',
                date: '2016-09-13 13:43',
                status: T.TaskStatus.Todo
              },
              {
                id: 2,
                name: 'taak 2',
                color: 'green',
                date: '2016-09-13 13:44',
                status: T.TaskStatus.Todo
              },
              {
                id: 3,
                name: 'taak 3',
                color: 'purple',
                date: '2016-09-13 13:44',
                status: T.TaskStatus.Todo
              },
              {
                id: 4,
                name: 'taak 4',
                color: 'black',
                date: '2016-09-13 13:46',
                status: T.TaskStatus.Todo
              },
            ]
          }
        ]
      },
      {
        keyframe: 5,
        data: [
          {
            id: 1,
            name: 'The first pbi',
            color: 'red',
            status: T.PbiStatus.Committed,
            date: '2016-09-12 10:00',
            tasks: [
              {
                id: 1,
                name: 'taak 1',
                color: 'blue',
                date: '2016-09-13 14:04',
                status: T.TaskStatus.InProgress
              },
              {
                id: 2,
                name: 'taak 2',
                color: 'green',
                date: '2016-09-13 14:06',
                status: T.TaskStatus.InProgress
              },
              {
                id: 3,
                name: 'taak 3',
                color: 'purple',
                date: '2016-09-13 13:44',
                status: T.TaskStatus.Todo
              },
              {
                id: 4,
                name: 'taak 4',
                color: 'black',
                date: '2016-09-13 13:46',
                status: T.TaskStatus.Todo
              },
            ]
          }
        ]
      },
      {
        keyframe: 6,
        data: [
          {
            id: 1,
            name: 'The first pbi',
            color: 'red',
            status: T.PbiStatus.Committed,
            date: '2016-09-12 10:00',
            tasks: [
              {
                id: 1,
                name: 'taak 1',
                color: 'blue',
                date: '2016-09-14 10:24',
                status: T.TaskStatus.Done
              },
              {
                id: 2,
                name: 'taak 2',
                color: 'green',
                date: '2016-09-13 14:06',
                status: T.TaskStatus.InProgress
              },
              {
                id: 3,
                name: 'taak 3',
                color: 'purple',
                date: '2016-09-13 13:44',
                status: T.TaskStatus.Todo
              },
              {
                id: 4,
                name: 'taak 4',
                color: 'black',
                date: '2016-09-13 13:46',
                status: T.TaskStatus.Todo
              },
            ]
          }

        ]
      },
      {
        keyframe: 7,
        data: [
          {
            id: 1,
            name: 'The first pbi',
            color: 'red',
            status: T.PbiStatus.Committed,
            date: '2016-09-12 10:00',
            tasks: [
              {
                id: 1,
                name: 'taak 1',
                color: 'blue',
                date: '2016-09-14 10:24',
                status: T.TaskStatus.Done
              },
              {
                id: 2,
                name: 'taak 2',
                color: 'green',
                date: '2016-09-14 17:06',
                status: T.TaskStatus.Todo
              },
              {
                id: 3,
                name: 'taak 3',
                color: 'purple',
                date: '2016-09-13 13:44',
                status: T.TaskStatus.Todo
              },
              {
                id: 4,
                name: 'taak 4',
                color: 'black',
                date: '2016-09-13 13:46',
                status: T.TaskStatus.Todo
              },
            ]
          }
        ]
      }
    ];
    this.highestKeyframe = _(this.timelapse).orderBy(x => x.keyframe).last().keyframe;
  }

  getKeyframeData(id) {
    return _(this.timelapse).find(x => x.keyframe === id).data;
  }
}
