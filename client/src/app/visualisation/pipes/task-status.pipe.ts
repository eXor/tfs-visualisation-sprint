import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'taskStatus'
})
export class TaskStatusPipe implements PipeTransform {

  transform(value: number) {
    switch (value) {
      case 0:
        return 'new';
      case 1:
        return 'in progress';
      case 2:
        return 'done';
      default:
        break;
    }
  }

}
