import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pbiStatus'
})
export class PbiStatusPipe implements PipeTransform {

  transform(value: number) {
    switch (value) {
      case 0:
        return 'new';
      case 1:
        return 'approved';
      case 2:
        return 'comitted';
      case 3:
        return 'done';
      default:
        break;
    }
  }

}
