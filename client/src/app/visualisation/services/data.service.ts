import { Injectable } from '@angular/core';
import * as T from '../../../models';

@Injectable()
export class DataService {
  constructor() { }

  Get(): Array<T.Pbi> {
    let dataset = new Array<T.Pbi>();

    dataset.push({
      id: 1,
      name: 'The first pbi',
      status: T.PbiStatus.New,
      date: new Date('2016-09-06 16:40'),
      tasks: []
    });

    dataset.push({
      id: 1,
      name: 'The first pbi',
      status: T.PbiStatus.Approved,
      date: new Date('2016-09-08 12:33'),
      tasks: []
    });

    dataset.push({
      id: 1,
      name: 'The first pbi',
      status: T.PbiStatus.Committed,
      date: new Date('2016-09-12 10:00'),
      tasks: []
    });

    dataset.push({
      id: 1,
      name: 'The first pbi',
      status: T.PbiStatus.Committed,
      date: new Date('2016-09-12 10:00'),
      tasks: [
        {
          id: 1,
          name: 'taak 1',
          date: new Date('2016-09-13 13:43'),
          status: T.TaskStatus.Todo
        },
        {
          id: 2,
          name: 'taak 2',
          date: new Date('2016-09-13 13:44'),
          status: T.TaskStatus.Todo
        },
        {
          id: 3,
          name: 'taak 3',
          date: new Date('2016-09-13 13:44'),
          status: T.TaskStatus.Todo
        },
        {
          id: 4,
          name: 'taak 4',
          date: new Date('2016-09-13 13:46'),
          status: T.TaskStatus.Todo
        },
        {
          id: 1,
          name: 'taak 1',
          date: new Date('2016-09-13 14:04'),
          status: T.TaskStatus.InProgress
        },
        {
          id: 2,
          name: 'taak 2',
          date: new Date('2016-09-13 14:06'),
          status: T.TaskStatus.InProgress
        },
        {
          id: 1,
          name: 'taak 1',
          date: new Date('2016-09-14 10:24'),
          status: T.TaskStatus.Done
        },
        {
          id: 2,
          name: 'taak 2',
          date: new Date('2016-09-14 17:06'),
          status: T.TaskStatus.Todo
        }
      ]
    });

    return;
  }

}
