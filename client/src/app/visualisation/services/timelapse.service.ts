import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';

import { DataService } from './data.service';
import * as T from '../../../models';

@Injectable()
export class TimelapseService {
  private data: Array<T.Pbi>;

  constructor(_dataService: DataService) {
    this.data = _dataService.Get();
  }

  ConvertToTimelapse(): Observable<T.Pbi> {
    return new Observable(obs => {
      // find first pbi
      let firstPbi = _(this.data)
        .orderBy(x => x.date)
        .first();

      // emit first pbi
      obs.next(firstPbi);

      // find date of first and last change
      let listOfDates = _(this.data)
        .map(x => {
          return [x.date, x.tasks.map(y => y.date)];
        })
        .flatten()
        .orderBy<any>(x => x.date);

      let firstDate = <number>listOfDates.first();
      let lastDate = <number>listOfDates.last();

      let secondsInBetween = (lastDate - firstDate) / 1000;

      // total time of timelapse (seconds)
      let timelapseTime = 30;
      // one frame duration measured in the data (seconds)
      let realLifeFrameDuration = secondsInBetween / timelapseTime;

      // emit every next pbi/task till the end

    });
  }
}
